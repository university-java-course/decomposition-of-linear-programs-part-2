package esde.tihomirov;

import java.util.Arrays;

public class Task2 {

    private String _add(String first, String second, String rest){
        return Integer.toString(Integer.parseInt(first, 10) + Integer.parseInt(second, 10) + Integer.parseInt(rest));
    }

    private String _getSector(String number, int startIndex){
        String substring = "0";
        if(number.length() > startIndex){
            substring = number.length() > startIndex + 1
                    ? number.substring(startIndex, startIndex + 1)
                    : number.substring(startIndex);
        }
        return substring;
    }

    private void _swap(char[] array, int i, int j){
        char temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    private String _reverseString(String str){
        char[] string = str.toCharArray();
        for (int i = 0; i < string.length/2; i++) {
            _swap(string, i, string.length - i - 1);
        }
        return String.valueOf(string);
    }

    public String addBigIntegers(String firstN, String secondN){
        String rest = "0";
        String result = "";
        String first = this._reverseString(firstN);
        String second = this._reverseString(secondN);
        for (int i = 0; i < Math.max(first.length(), second.length()); i += 1) {
            String firstSubstring = this._getSector(first, i);
            String secondSubstring = this._getSector(second, i);
            String res = this._add(firstSubstring, secondSubstring, rest);
            rest = res.length() > 1 ? res.substring(0, 1) : "0";
            String number = res.length() > 1  ? res.substring(1) : res.substring(0);
            result = String.join("", number, result);
        }
        return result;
    }
}
