package esde.tihomirov;

import java.util.Arrays;

public class Task4 {

    public int sum(int[]array, int end){
        int sum = 0;
        for (int i = end-2; i <= end; i++) {
            sum += array[i];
        }
        return sum;
    }

    public int[] sumsFromKtoM(int[]array, int k, int m){
        int end = k + 2;
        int[] sums = new int[m-k];
        int size = 0;
        while (end < m){
            sums[size] = sum(array, end);
            end += 3;
            size++;
        }
        sums[size] = sum(array, m);
        size++;
        return Arrays.copyOf(sums, size);
    }
}
