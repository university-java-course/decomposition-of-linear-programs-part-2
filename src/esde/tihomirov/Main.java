package esde.tihomirov;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Task1 task1 = new Task1();
        System.out.println(task1.isCoprime(4,5,13));

        Task2 task2 = new Task2();
        String first = "45367890564322156785463789543789554326789543263789";
        String second = "2435678965423678543267845687865474855475547";
        System.out.printf("%70s\n%70s\n%70s\n\n", first, second, task2.addBigIntegers(first, second));

        Task3 task3 = new Task3();
        System.out.println(Arrays.toString(task3.findArmstrongNumbers(100)));

        Task4 task4 = new Task4();
        int[] array = new int[]{4, 5, 7,54,2,76,3,7,3};
        System.out.println(Arrays.toString(task4.sumsFromKtoM(array, 0, 7)));
    }
}
