package esde.tihomirov;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Task3 {

    private int _getCountOfDigits(int number){
        return Integer.toString(number).length();
    }

    public boolean isArmstrongNumber(int number){
        int size = this._getCountOfDigits(number);
        char[] digits = Integer.toString(number).toCharArray();
        int sum = 0;
        for (char digit: digits) {
            sum += Integer.parseInt(String.valueOf(digit));
        }
        int res = (int) Math.pow(sum, size);
        return res == number;
    }

    public int[] findArmstrongNumbers(int k){
        int[] temp = new int[k];
        int size = 0;
        for (int i = 1; i <= k; i++) {
            if(this.isArmstrongNumber(i)){
                temp[size] = i;
                size++;
            }
        }
        int[] result = Arrays.copyOf(temp, size);
        return result;
    }
}
