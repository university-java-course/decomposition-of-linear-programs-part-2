package esde.tihomirov;

public class Task1 {
    public int greatestCommonDivisor(int first, int second) {
        while (first != 0 && second != 0){
            if (first > second){
                first = first % second;
            } else {
                second = second % first;
            }
        }
        return first + second;
    }

    public boolean isCoprime(int first, int second, int third){
        return this.greatestCommonDivisor(first, second) == 1
                && this.greatestCommonDivisor(first, third) == 1
                && this.greatestCommonDivisor(second, third) == 1;
    }
}
